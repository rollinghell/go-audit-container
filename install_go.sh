# DO NOT RUN WITH SUDO

sudo apt-get -y install build-essential
cd /tmp/
wget https://dl.google.com/go/go1.11.linux-amd64.tar.gz
tar -xzvf go1.11.linux-amd64.tar.gz 
sudo mv go /usr/local
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
go get -u github.com/kardianos/govendor
go get -u github.com/mitchellh/go-ps
